"use strict";

const
    asynchronous = require('async'),
    bcrypt = require('bcrypt'),
    jwt = require('jsonwebtoken'),
    moment = require('moment-timezone'),
    requireDirectory = require('require-directory');

// Define Modules
const
    constants = requireDirectory(module, '../../../constants'),
    models = requireDirectory(module, '../../../models'),
    utils = requireDirectory(module, '../../../../utils');

const
    tripStatus = constants.enumTripStatus,

    Driver = models.driver,
    DriverLogisticProviderConfig = models.driverLogisticProviderConfig,
    Trip = models.trip,

    Response = utils.responseByErrorCode,
    Validate_Request = utils.validate_request;

const
    functionName = 'api/services/driver/login';

module.exports = function (requisition, loggerObject) {
    return new Promise((resolve, reject) => {
        loggerObject
            .config({
                "function_name": functionName
            })
            .msg([{
                "level": "debug",
                "log": ["login service in progress ..."]
            }]);

        var driver = {};
        var res = {};

        const app = requisition.body.app;
        const battery = requisition.body.battery;
        const device = requisition.body.device;
        const headers = requisition.headers;
        const federalTaxId = headers.login;
        const logisticProviderId = headers.logistic_provider_id;

        var resValidate = new Validate_Request(requisition).verify(
            'login',
            'password',
            'body_app',
            'logistic_provider_id'
        );

        if (resValidate.length) {
            loggerObject
                .msg([{
                    "level": "warn",
                    "log": ["there is data that has not been provided, " + resValidate]
                }]);

            res = Response.error_code({
                'code': 'E1013'
            });

            return reject(res);
        }

        asynchronous.series({
            validatingDriver: function (callback) {
                loggerObject
                    .msg([{
                        "level": "debug",
                        "log": ["validating driver ..."]
                    }]);

                Driver.selectByFederalTaxId(federalTaxId)
                    .then(result => {
                        if (!result) {
                            loggerObject
                                .msg([{
                                    "level": "warn",
                                    "log": ["login is not valid, " + federalTaxId]
                                }]);

                            res = Response.error_code({
                                'code': 'E1000'
                            });

                            return reject(res);
                        } else {
                            loggerObject
                                .msg([{
                                    "level": "debug",
                                    "log": ["the user exist with login"]
                                }]);

                            let compareResult = bcrypt.compareSync(headers.password, result.dr_password);

                            if (!compareResult) {
                                loggerObject
                                    .msg([{
                                        "level": "warn",
                                        "log": ["password is not valid"]
                                    }]);

                                res = Response.error_code({
                                    'code': 'E1000'
                                });

                                return reject(res);
                            } else {
                                driver = result;

                                loggerObject
                                    .config({
                                        "driver_id": driver.dr_id
                                    })
                                    .msg([{
                                        "level": "debug",
                                        "log": ["validating driver ok"]
                                    }]);

                                callback(null, 'ok');
                            }
                        }
                    }).catch(error => {
                        loggerObject
                            .msg([{
                                "level": "error",
                                "log": ["could not get driver through the field dr_federal_tax_id, " + error.message]
                            }]);

                        res = Response.error_code({
                            'code': 'E1003'
                        });

                        return reject(res);
                    });
            },

            validatingDriverLogisticProvider: function (callback) {
                loggerObject
                    .level('debug')
                    .log('verifying the relationship between driver and logistic provider');

                DriverLogisticProviderConfig.validate(driver.dr_id, logisticProviderId)
                    .then(result => {
                        if (!result) {
                            loggerObject
                                .msg([{
                                    "level": "warn",
                                    "log": ["driver is not related with logistic provider: " + logisticProviderId]
                                }]);

                            res = Response.error_code({
                                'code': 'E1005'
                            });

                            return reject(res);
                        } else {
                            loggerObject
                                .config({
                                    "logistic_provider_id": logisticProviderId
                                })
                                .msg([{
                                    "level": "debug",
                                    "log": ["driver and logistic provider are related"]
                                }]);

                            callback(null, 'ok');
                        }
                    }).catch(error => {
                        loggerObject
                            .msg([{
                                "level": "error",
                                "log": ["could not get driver_logistic_provider_config through the field dlpc_driver_id and dlpc_logistic_provider_id, " + error.message]
                            }]);

                        res = Response.error_code({
                            'code': 'E1003'
                        });

                        return reject(res);
                    });
            },

            checkLoggedIn: function (callback) {
                loggerObject
                    .msg([{
                        "level": "debug",
                        "log": ["checking if user already is connect ..."]
                    }]);

                if (!driver.dr_logged_in) {
                    callback(null, 'ok');
                } else {
                    if (driver.dr_device_uuid == device.device_uuid) {
                        loggerObject
                            .msg([{
                                "level": "debug",
                                "log": ["driver is already connected to a device, but is this same device"]
                            }]);

                        callback(null, 'ok');
                    } else {
                        Trip.selectToLogin(driver.dr_id, tripStatus.STARTED)
                            .then(result => {
                                if (!result.count) {
                                    loggerObject
                                        .msg([{
                                            "level": "debug",
                                            "log": ["driver is not connected to another device"]
                                        }]);

                                    callback(null, 'ok');
                                } else {
                                    loggerObject
                                        .msg([{
                                            "level": "warn",
                                            "log": ["the driver has started trips, so, you can not sign in to this device"]
                                        }]);

                                    res = Response.error_code({
                                        'code': 'E1006'
                                    });

                                    return reject(res);
                                }
                            }).catch(error => {
                                loggerObject
                                    .msg([{
                                        "level": "error",
                                        "log": ["could not get trip through the fields tr_driver_id and tr_status_id, " + error.message]
                                    }]);

                                res = Response.error_code({
                                    'code': 'E1003'
                                });

                                return reject(res);
                            });
                    }
                }
            },

            updateDriver: function (callback) {
                loggerObject
                    .msg([{
                        "level": "debug",
                        "log": ["updating driver ..."]
                    }]);

                const modified = moment().utc().format();

                if (driver.dr_is_blocked) {
                    loggerObject
                        .msg([{
                            "level": "warn",
                            "log": ["driver is blocked"]
                        }]);

                    res = Response.error_code({
                        'code': 'E1009'
                    });

                    return reject(res);
                }

                var token;

                if (!driver.dr_token) {
                    loggerObject
                        .msg([{
                            "level": "debug",
                            "log": ["it is the first sign of the user"]
                        }]);

                    token = 'dr+' + jwt.sign({
                        user: driver.dr_federal_tax_id
                    }, driver.dr_cell_phone);
                } else {
                    loggerObject
                        .msg([{
                            "level": "debug",
                            "log": ["it is not the first sign of the user"]
                        }]);

                    token = driver.dr_token;
                }

                let date = moment().utc().format();

                if (!device) {
                    device = {};
                }

                if (!battery) {
                    battery = {};
                }

                let obj = {};

                obj.dr_app_version = app.version;
                obj.dr_configuration = app.configuration;
                obj.dr_device_platform = device.platform;
                obj.dr_platform_version = device.platform_version;
                obj.dr_device_manufacturer = device.device_manufacturer;
                obj.dr_device_model = device.device_model;
                obj.dr_device_uuid = device.device_uuid;
                obj.dr_network_provider = device.network_provider;
                obj.dr_push_id = device.push_id;
                obj.dr_token = token;
                obj.dr_logged_in = true;
                obj.dr_last_login = date;
                obj.dr_modified = modified;
                obj.dr_current_logistic_provider_id = logisticProviderId;
                obj.dr_federal_tax_id = headers.login;

                Driver.updateLoginByFederalTaxId(obj)
                    .then(result => {
                        driver = result;
                        loggerObject
                            .msg([{
                                "level": "debug",
                                "log": ["updating driver ok"]
                            }]);

                        callback(null, 'ok');
                    }).catch(error => {
                        loggerObject
                            .msg([{
                                "level": "error",
                                "log": ["could not update driver through the fields dr_federal_tax_id, " + error.message]
                            }]);

                        res = Response.error_code({
                            'code': 'E1003'
                        });

                        return reject(res);
                    });
            }

        }, function (error, results) {
            if (!error) {
                loggerObject
                    .msg([{
                        "level": "debug",
                        "log": ["login services in progress ok"]
                    }]);

                res = Response.error_code({
                    'code': 'E200'
                });

                res.driver = driver;
                return resolve(res);
            }
        });
    });
}